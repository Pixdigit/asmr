extends Area2D

func _ready():
	pass


func _on_Left_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed:
		if get_node("/root/Root").find_node("Book").visible:
			get_parent().find_node("Cards").previous()
