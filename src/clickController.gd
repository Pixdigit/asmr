extends Node

func _ready():
	pass


func _on_ExitButton_up():
	get_tree().quit()


func _on_StartButton_up():
	self.visible = false
	get_parent().find_node("Game").visible = true
	get_parent().find_node("Dialog").visible = true
	get_parent().find_node("Camera2D").current = true
