extends Control

const base = 3

signal finished_game

var rng = RandomNumberGenerator.new()

var shapes = [
	line, cross, t, s, l
]

var shape_positions = [
	line_positions, cross_positions, t_positions, s_positions, l_positions
]

var poss = []
var numbers = []

const line = [
	[1, 1, 1, 1],
]

const cross = [
	[0, 1],
	[1, 1, 1],
	[0, 1]
]

const t = [
	[1],
	[1, 1],
	[1]
]

const s = [
	[1],
	[1, 1],
	[0, 1]
]

const l = [
	[1, 1],
	[1]
]



const line_positions = [
	Vector2(0, 0),
	Vector2(0, 1),
	Vector2(0, 2),
	Vector2(0, 3)
	]

const cross_positions = [
	Vector2(0, 0), Vector2(1, 0),
	Vector2(0, 1), Vector2(1, 1),
	]

const t_positions = [
	Vector2(0, 0), Vector2(1, 0), Vector2(2, 0),
	Vector2(0, 1), Vector2(1, 1), Vector2(2, 1),
	]

const s_positions = [
	Vector2(0, 0), Vector2(1, 0), Vector2(2, 0),
	Vector2(0, 1), Vector2(1, 1), Vector2(2, 1),
	]

const l_positions = [
	Vector2(0, 0), Vector2(1, 0), Vector2(2, 0),
	Vector2(0, 1), Vector2(1, 1), Vector2(2, 1),
	Vector2(0, 2), Vector2(1, 2), Vector2(2, 2),
	]

var obj = null
var fun = ""



func gen_rands(poss):
	for row in range(poss.size()):
		for colum in range(poss[row].size()):
			if poss[row][colum] != 0:
				poss[row][colum] = rng.randi_range(1, base)
	return poss


func _ready():
	rng.randomize()
	gen_numbers()

func shuffle_shapes():
	var box = self.get_node("../../ShapeBox/Shapes")
	for child in box.get_children():
		child.place_random()


func callback(obj, fun):
	obj = obj
	fun = fun


func gen_numbers():
	var line_pos = line_positions[rng.randi_range(0, line_positions.size() - 1)]
	var cross_pos = cross_positions[rng.randi_range(0, cross_positions.size() - 1)]
	var t_pos = t_positions[rng.randi_range(0, t_positions.size() - 1)]
	var s_pos = s_positions[rng.randi_range(0, s_positions.size() - 1)]
	var l_pos = l_positions[rng.randi_range(0, l_positions.size() - 1)]

	poss = [line_pos, cross_pos, t_pos, s_pos, l_pos]

	for i in range(shapes.size()):
		shapes[i] = gen_rands(shapes[i])

	numbers = []
	for y in range(4):
		for x in range(4):
			var value = shapes.size() * base
			var pos = Vector2(x, y)

			for i in shapes.size():
				var rel_pos = pos - poss[i]
				var shape = shapes[i]
				if shape.size() > rel_pos.y and rel_pos.y >= 0:
					var lane = shape[rel_pos.y]
					if lane.size() > rel_pos.x and rel_pos.x >= 0:
						value -= lane[rel_pos.x]
			numbers.push_back(value)

	for i in range(numbers.size()):
		var x = i / 4
		var y = i % 4
		var numberLabel : Label = self.find_node("Label" + str(x) + str(y))
		numberLabel.text = str(numbers[i])

	for i in range(shapes.size()):
		var shape = shapes[i]
		if i == 0:
			for i2 in range(shape[0].size()):
				self.get_node("../../ShapeBox/Shapes/Line/Line").get_child(i2).text = str(shapes[i][0][i2])

		elif i == 1:
			for i2 in range(5):
				var value = 0
				if i2 == 0:
					value = shape[0][1]
				elif i2 == 1:
					value = shape[1][0]
				elif i2 == 2:
					value = shape[1][1]
				elif i2 == 3:
					value = shape[1][2]
				elif i2 == 4:
					value = shape[2][1]
				self.get_node("../../ShapeBox/Shapes/Cross/Cross").get_child(i2).text = str(value)

		elif i == 2:
			for i2 in range(4):
				var value = 0
				if i2 == 0:
					value = shape[0][0]
				elif i2 == 1:
					value = shape[1][0]
				elif i2 == 2:
					value = shape[1][1]
				elif i2 == 3:
					value = shape[2][0]
				self.get_node("../../ShapeBox/Shapes/T/T").get_child(i2).text = str(value)

		elif i == 3:
			for i2 in range(4):
				var value = 0
				if i2 == 0:
					value = shape[0][0]
				elif i2 == 1:
					value = shape[1][0]
				elif i2 == 2:
					value = shape[1][1]
				elif i2 == 3:
					value = shape[2][1]
				self.get_node("../../ShapeBox/Shapes/S/S").get_child(i2).text = str(value)

		elif i == 4:
			for i2 in range(3):
				var value = 0
				if i2 == 0:
					value = shape[0][0]
				elif i2 == 1:
					value = shape[0][1]
				elif i2 == 2:
					value = shape[1][0]
				self.get_node("../../ShapeBox/Shapes/L/L").get_child(i2).text = str(value)
	tally()


func tally():
	var goal = shapes.size() * base
	var result = []
	for y in range(4):
		for x in range(4):
			var value = numbers[y * 4 + x]
			var pos = Vector2(x, y)

			for i in shapes.size():
				var box = self.get_node("../../ShapeBox/Shapes").get_children()[i]
				if box.is_snapped:
					var rel_pos = pos - box.grid_pos
					var shape = shapes[i]
					if shape.size() > rel_pos.y and rel_pos.y >= 0:
						var lane = shape[rel_pos.y]
						if lane.size() > rel_pos.x and rel_pos.x >= 0:
							value += lane[rel_pos.x]
			var child = self.find_node("Label" + str(y) + str(x))
			if value == goal:
				child.set("custom_colors/font_color", Color(0, 1, 0))
			elif value > goal:
				child.set("custom_colors/font_color", Color(1, 0, 0))
			else:
				child.set("custom_colors/font_color", Color(0, 0, 0))

			result.push_back(value)
	for value in result:
		if value != goal:
			return

	self.get_node("../../").visible = false
	self.get_node("/root/Root/Game").find_node("Camera2D").current = true
	
	self.emit_signal("finished_game")
