extends Area2D

export(Vector2) var dest_tile = Vector2()
export(String) var spirit = ""
export(String) var tile_name = ""

var wait_for_confirm = false

var book_visible = false


func _ready():
	pass

func _input(event):
	if wait_for_confirm and (
			(event is InputEventMouseButton and event.button_index == BUTTON_LEFT)
				or 
			(event is InputEventKey and event.is_action("interact"))
		):

			if self.get_node("/root/Root/Game/").find_node("DialogBox").i == 3:
				self.get_node("/root/Root/Game/").find_node("DialogBox").i += 1
				self.get_node("/root/Root/Game/").find_node("DialogBox").handle_continue()

			var puzzle = self.get_node("/root/Root/Puzzle1")
			var nums = puzzle.get_node("Field/Numbers")
			nums.gen_numbers()
			nums.shuffle_shapes()
			nums.connect("finished_game", self, "update_sprite")
			
			for child in puzzle.get_node("ShapeBox/Shapes").get_children():
				child.unsnap()

			self.get_node("/root/Root/MainCamera").current = true
			wait_for_confirm = false
			puzzle.visible = true

	if event.is_action_pressed("close"):
		book_visible = false
		get_node("/root/Root").find_node("Book").visible = false
		get_node("/root/Root/AudioStreamPlayer").stream_paused = false
		get_node("/root/Root/AudioStreamPlayer2").stream_paused = true
	
	#if not book_visible:
	#	get_node("/root/Root").find_node("Book").visible = false
	#else:
	#	get_node("/root/Root").find_node("Book").visible = true



func update_sprite():
	var map = get_node("/root/Root").find_node("Deko")
	map.set_cell(dest_tile.x, dest_tile.x, map.tile_set.find_tile_by_name(tile_name))
	get_node("/root/Root").find_node("Book").visible = true
	book_visible = true

	get_node("/root/Root/AudioStreamPlayer").stream_paused = true
	get_node("/root/Root/AudioStreamPlayer2").stream_paused = false
	get_node("/root/Root").find_node("Cards").spirits.push_back(spirit)

func _on_stove_body_entered(body):
	if body.name == "Character":
		wait_for_confirm = true

func _on_stove_body_exited(body):
	if body.name == "Character":
		wait_for_confirm = false
