extends KinematicBody2D

export(float) var MOTION_SPEED = 160 # Pixels/second

onready var char_anim = self.get_node("AnimatedSprite")

var rng = RandomNumberGenerator.new()

var motion = Vector2(0, 0)
var moving = false
var moving_right = true
var has_apron = false

var found_sprits = ["Hierophant"]

var move_enabled = false

var blink_timer = Timer.new()
var undo_timer = Timer.new()


func _ready():
	undo_timer.one_shot = true
	undo_timer.wait_time = 0.2
	undo_timer.connect("timeout", self, "unblink")

	blink_timer.wait_time = rng.randf_range(4.0, 6.0)
	blink_timer.connect("timeout", self, "blink")
	self.add_child(blink_timer)
	self.add_child(undo_timer)
	blink_timer.start()

func blink():
	undo_timer.start()
	blink_timer.wait_time = rng.randf_range(4.0, 6.0)

	var frame = char_anim.frame
	if has_apron:
		char_anim.animation = "walk_eyes_closed_apron"
	else:
		char_anim.animation = "walk_eyes_closed"
	char_anim.frame = frame

func unblink():
	var frame = char_anim.frame
	if has_apron:
		char_anim.animation = "walk_apron"
	else:
		char_anim.animation = "walk"
	char_anim.frame = frame

func _input(event):
	if event is InputEventKey and not event.is_echo():
		if event.is_action_pressed("walk_up"):
			motion += Vector2(0, -1)
		if event.is_action_released("walk_up"):
			motion -= Vector2(0, -1)

		if event.is_action_pressed("walk_down"):
			motion += Vector2(0, 1)
		if event.is_action_released("walk_down"):
			motion -= Vector2(0, 1)

		if event.is_action_pressed("walk_left"):
			motion += Vector2(-1, 0)
		if event.is_action_released("walk_left"):
			motion -= Vector2(-1, 0)

		if event.is_action_pressed("walk_right"):
			motion += Vector2(1, 0)
		if event.is_action_released("walk_right"):
			motion -= Vector2(1, 0)

		if motion.x > 0:
			if !moving_right:
				moving_right = true
				self.apply_scale(Vector2(-1, 1))
		elif motion.x < 0:
			if moving_right:
				moving_right = false
				self.apply_scale(Vector2(-1, 1))
	
		if motion == Vector2(0, 0):
			if moving:
				moving = false
				find_node("AnimatedSprite").playing = false
				find_node("AnimatedSprite").frame = 3
		else:
			if !moving:
				moving = true
				find_node("AnimatedSprite").playing = true
				find_node("AnimatedSprite").frame = 0

func _physics_process(delta):
	if move_enabled:
		move_and_slide(motion.normalized() * MOTION_SPEED)


func _show_wrench(body):
	if body.name == "Character":
		get_node("AnimatedSprite2").animation = "wrench"
		get_node("AnimatedSprite2").visible = true
func _show_grab(body):
	if body.name == "Character":
		get_node("AnimatedSprite2").animation = "grab"
		get_node("AnimatedSprite2").visible = true
func _hide_interact(body):
	if body.name == "Character":
		get_node("AnimatedSprite2").visible = false


func wear_apron():
	has_apron = true
	self.get_node("AnimatedSprite").animation = "walk_apron"
func _drop_apron():
	self.get_node("AnimatedSprite").animation = "walk"
