extends Label

var texts = [
"""\"Gramps died about two years ago.
It is now that I can finally inherit his shop.
I, Milanka Bellincioni, will finally be able to prove myself.
I will be the greatest Ramen cook ever!!!\"""",

"""\"But this place is a right mess.
And almost everything is broken.
*sigh* 
Let’s get to work.\"""",

"""Move using WASD.
Click the left mouse button or press space to interact with an object
when the fixing symbol appears on your character.""",

"",
"""\"Did someone break in and…?
Seriously, how do you even…?
Let’s fix you up lil’ buddy.\"""",

"""To fix a thing, you must add parts to it where its broken. 
On the left side you see the box that represents the item to be fixed. 
On the right side you see the parts with which you do it.""",

"""When you drag a part from the right to the left,
you will see that it aligns to the grid on the left.
It also adds its values to the numbers of the left grid.
Try it!""","",

"""Your goal is to place the parts in a way where each space on the left has the same value. 
Green numbers indicate that this space has the right number.
Red Numbers indicate that this space’s value is too high.
Happy fixing!
"""
]

var i = 1


func _ready():
	self.text = texts[0]
	pass
	
func _on_Panel_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT && event.pressed:
			handle_continue()

func _input(event):
	if event is InputEventKey and event.is_action_pressed("interact"):
		handle_continue()

func handle_continue():
	if texts.size() > i:
		if i == 2:
			get_node("/root/Root").find_node("Character").move_enabled = true
		if i != 3 and i != 7:
			self.text = texts[i]
			i += 1
	else:
		get_node("/root/Root/").find_node("Dialog").visible = false
