extends Node2D

var wait_for_confirm = false

func _ready():
	pass

func _input(event):
	if wait_for_confirm and (
			(event is InputEventMouseButton and event.button_index == BUTTON_LEFT)
				or 
			(event is InputEventKey and event.is_action("interact"))
		):
			get_parent().find_node("Character").wear_apron()
			get_parent().find_node("Character")._hide_interact(get_parent().find_node("Character"))
			self.queue_free()

func _on_Area2D_body_entered(body):
	if body.name == "Character":
		wait_for_confirm = true
		
func _on_Area2D_body_exited(body):
	if body.name == "Character":
		wait_for_confirm = false
