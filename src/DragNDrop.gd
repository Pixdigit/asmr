extends Node2D

export(int, 0, 44) var shape = 0

const snap_width = 37

onready var rng = RandomNumberGenerator.new()

var is_dragging = false
var mouse_in = false
var mouse_offset = Vector2(0, 0)

var field_tl = Vector2(105, 165)
var field_br = Vector2(855, 915)
var snap_tl = field_tl - Vector2(93, 93)
var snap_br = field_br + Vector2(93, 93)

var is_snapped = false
var grid_pos = Vector2(-1, -1)

func _ready():
	rng.randomize()

func place_random():
	var offset = Vector2(960 - 187, -187)
	var tl = field_tl + offset
	var br = field_br + offset - Vector2(187, 187)
	var x = rng.randi_range(tl.x, br.x)
	var y = rng.randi_range(tl.y, br.y)
	self.position = Vector2(x, y)

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed and mouse_in:
			mouse_offset = event.global_position -  self.global_position
			is_dragging = true
			unsnap()
			get_tree().set_input_as_handled()
		if not event.pressed:
			if (snap_tl.x < self.position.x && self.position.x < snap_br.x
				and
				snap_tl.y < self.position.y && self.position.y < snap_br.y):
					snap()
			is_dragging = false

func _physics_process(delta):
	if is_dragging:
		self.position = get_viewport().get_mouse_position() - mouse_offset

func snap():
	get_node("/root/Root").find_node("DialogBox").i += 1
	get_node("/root/Root").find_node("DialogBox").handle_continue()
	
	
	var pos = self.position
	pos += Vector2(70, 70)
	pos -= field_tl
	pos.x = int(pos.x) / 186
	pos.y = int(pos.y) / 186
	grid_pos = pos
	for p in self.get_node("../../../Field/Numbers").shape_positions[shape]:
		if pos == p:
			is_snapped = true
			pos *= 186
			pos += field_tl
			self.position = pos
			for child in self.get_node(self.name).get_children():
				child.get("custom_fonts/font").size = 65
				child.set("custom_colors/font_color", self.get_node("Outline").outline)
				child.margin_left = shape * snap_width
				child.margin_bottom =  -128
				child.margin_right = shape * snap_width - 187
			self.get_node("../../../Field/Numbers").tally()


func unsnap():
	is_snapped = false
	self.get_node("../../../Field/Numbers").tally()
	for child in self.get_node(self.name).get_children():
		child.get("custom_fonts/font").size = 130
		child.margin_left = 0
		child.margin_bottom =  0
		child.margin_right = 0


func on_mouse_enter():
	mouse_in = true
func on_mouse_leave():
	mouse_in = false
