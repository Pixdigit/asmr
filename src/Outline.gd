tool
extends Polygon2D

export(Color) var outline = Color(0,0,0) setget set_color
export(float) var width = 10.0 setget set_width

func _draw():
	var poly = self.get_polygon()
	self.draw_polyline(poly, outline, width, true)

func set_color(color):
	outline = color
	self.update()

func set_width(new_width):
	width = new_width
	self.update()
