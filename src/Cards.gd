extends Control

var spirits = []
var current = ""

func _ready():
	pass

func previous():
	var start = current
	var after = (current == "")
	for child in self.get_children():
		if spirits.find(child.name) != -1 && after:
			current = child.name
			break
		if child.name == current:
			after = true
	if current == start:
		current = ""
	update()

func next():
	var start = current
	var before = (current == "")
	for j in range(self.get_children().size()):
		var i = self.get_children().size() - j - 1
		var child = self.get_child(i)
		if spirits.find(child.name)!= -1&& before:
			current = child.name
			break
		if child.name == current:
			before = true
	if current == start:
		current = ""
	update()


func update():
	if current == "":
		get_parent().get_node("Greeting").visible = true
	else:
		get_parent().get_node("Greeting").visible = false

	for child in self.get_children():
		if child.name == current:
			child.visible = true
		else:
			child.visible = false
